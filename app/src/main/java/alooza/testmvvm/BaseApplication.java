package alooza.testmvvm;

import android.app.Application;
import android.content.Context;

import alooza.testmvvm.network.CountryService;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by Azim on 05/01/2017.
 */

public class BaseApplication extends Application {

    private CountryService countryService;
    private Scheduler defaultSubscribeScheduler;

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public CountryService getCountryService() {
        if (countryService == null) {
            countryService = CountryService.Factory.create();
        }
        return countryService;
    }

    //For setting mocks during testing
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }
    //User to change scheduler from tests
    public void setDefaultSubscribeScheduler(Scheduler scheduler) {
        this.defaultSubscribeScheduler = scheduler;
    }
}
