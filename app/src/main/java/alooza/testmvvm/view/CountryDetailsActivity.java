package alooza.testmvvm.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import alooza.testmvvm.R;
import alooza.testmvvm.databinding.ActivityCountryDetailsBinding;
import alooza.testmvvm.model.Country;
import alooza.testmvvm.viewmodel.CountryDetailsViewModel;

public class CountryDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_COUNTRY = "EXTRA_COUNTRY";

    private ActivityCountryDetailsBinding binding;
    private CountryDetailsViewModel countryDetailsViewModel;

    public static Intent newIntent(Context context, Country country) {
        Intent intent = new Intent(context, CountryDetailsActivity.class);
        intent.putExtra(EXTRA_COUNTRY, country);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_country_details);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Country country = getIntent().getParcelableExtra(EXTRA_COUNTRY);
        countryDetailsViewModel = new CountryDetailsViewModel(this, country);
        binding.setViewModel(countryDetailsViewModel);

        setTitle(country.name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countryDetailsViewModel.destroy();
    }
}
