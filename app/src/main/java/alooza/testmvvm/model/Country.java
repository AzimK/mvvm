package alooza.testmvvm.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Azim on 05/01/2017.
 */

public class Country implements Parcelable {

    public String name;
    public String capital;
    public String region;
    public String subRegion;
    public int population;

    public Country () {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.capital);
        parcel.writeString(this.region);
        parcel.writeString(this.subRegion);
        parcel.writeInt(population);
    }

    protected Country(Parcel in) {
        this.name = in.readString();
        this.capital = in.readString();
        this.region = in.readString();
        this.subRegion = in.readString();
        this.population = in.readInt();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        public Country createFromParcel(Parcel source) {
            return new Country(source);
        }

        public Country[] newArray(int size) {
            return new Country[size];
        }
    };
}
