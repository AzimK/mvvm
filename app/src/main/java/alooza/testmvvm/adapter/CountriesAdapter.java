package alooza.testmvvm.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import alooza.testmvvm.R;
import alooza.testmvvm.databinding.ItemCountryBinding;
import alooza.testmvvm.model.Country;
import alooza.testmvvm.viewmodel.ItemViewModel;

/**
 * Created by Azim on 05/01/2017.
 */

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.CountryViewHolder>{

    private List<Country> countries;

    public CountriesAdapter() {
        this.countries = Collections.emptyList();
    }

    public CountriesAdapter(List<Country> countries) {
        this.countries = countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCountryBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_country,
                parent,
                false);
        return new CountryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        holder.bindRepository(countries.get(position));
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public static class CountryViewHolder extends RecyclerView.ViewHolder {
        final ItemCountryBinding binding;

        public CountryViewHolder(ItemCountryBinding binding) {
            super(binding.cardView);
            this.binding = binding;
        }

        void bindRepository(Country country) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemViewModel(itemView.getContext(), country));
            } else {
                binding.getViewModel().setCountry(country);
            }
        }
    }

}
