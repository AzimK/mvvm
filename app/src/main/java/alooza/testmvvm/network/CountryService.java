package alooza.testmvvm.network;

import java.util.List;

import alooza.testmvvm.model.Country;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Azim on 05/01/2017.
 */

public interface CountryService {

    @GET("rest/v1/name/{query}")
    Observable<List<Country>> country(@Path("query") String query);

    @GET("rest/v1/name")
    Call<Country> getCountry();

    class Factory {
        public static CountryService create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://restcountries.eu/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(CountryService.class);
        }
    }

}
