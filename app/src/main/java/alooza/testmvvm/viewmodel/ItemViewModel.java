package alooza.testmvvm.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.view.View;
import android.widget.Toast;

import alooza.testmvvm.model.Country;
import alooza.testmvvm.view.CountryDetailsActivity;

/**
 * Created by Azim on 05/01/2017.
 */

public class ItemViewModel extends BaseObservable implements ViewModel {

    private Country country;
    private Context context;

    public ItemViewModel(Context context, Country country) {
        this.country = country;
        this.context = context;
    }

    public String getName() {
        return country.name;
    }

    public String getCapital() {
        return country.capital;
    }

    public void onItemClick(View view) {
        context.startActivity(CountryDetailsActivity.newIntent(context, country));
    }

    // Allows recycling ItemViewModels within the recyclerview adapter
    public void setCountry(Country country) {
        this.country = country;
        notifyChange();
    }

    @Override
    public void destroy() {

    }
}
