package alooza.testmvvm.viewmodel;

import android.content.Context;
import android.databinding.ObservableInt;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.List;

import alooza.testmvvm.BaseApplication;
import alooza.testmvvm.model.Country;
import alooza.testmvvm.network.CountryService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Azim on 05/01/2017.
 */

public class MainViewModel implements ViewModel {

    private static final String TAG = "MainViewModel";

    public ObservableInt progressVisibility;
    public ObservableInt recyclerViewVisibility;
    public ObservableInt searchButtonVisibility;

    private Context context;
    private Subscription subscription;
    private List<Country> countries;
    private DataListener dataListener;
    private String editTextQueryValue;

    public MainViewModel(Context context, DataListener dataListener) {
        this.context = context;
        this.dataListener = dataListener;
        this.progressVisibility = new ObservableInt(View.GONE);
        this.recyclerViewVisibility = new ObservableInt(View.GONE);
        this.searchButtonVisibility = new ObservableInt(View.GONE);
    }

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        context = null;
        dataListener = null;
    }

    public boolean onSearchAction(TextView view, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String query = view.getText().toString();
            if (query.length() > 0) loadCountry(query);
            return true;
        }
        return false;
    }

    public void onClickSearch(View view) {
        loadCountry(editTextQueryValue);
    }

    public TextWatcher getUsernameEditTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                editTextQueryValue = charSequence.toString();
                searchButtonVisibility.set(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private void loadCountry(String query) {
        progressVisibility.set(View.VISIBLE);
        recyclerViewVisibility.set(View.INVISIBLE);

        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        BaseApplication application = BaseApplication.get(context);
        CountryService countryService = application.getCountryService();
        subscription = countryService.country(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<List<Country>>() {
                    @Override
                    public void onCompleted() {
                        if (dataListener != null) dataListener.onRepositoriesChanged(countries);
                        progressVisibility.set(View.INVISIBLE);
                        if (!countries.isEmpty()) {
                            recyclerViewVisibility.set(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Error loading GitHub repos ", e);
                        progressVisibility.set(View.INVISIBLE);
                    }

                    @Override
                    public void onNext(List<Country> countries) {
                        Log.i(TAG, "Repos loaded " + countries);
                        MainViewModel.this.countries = countries;
                    }
                });

        countryService.getCountry().enqueue(new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {

            }
        });
    }

    private static boolean isHttp404(Throwable error) {
        return error instanceof HttpException && ((HttpException) error).code() == 404;
    }

    public interface DataListener {
        void onRepositoriesChanged(List<Country> repositories);
    }
}
