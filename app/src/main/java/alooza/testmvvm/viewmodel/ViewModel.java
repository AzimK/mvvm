package alooza.testmvvm.viewmodel;

public interface ViewModel {
    void destroy();
}