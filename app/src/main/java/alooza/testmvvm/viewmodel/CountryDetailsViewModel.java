package alooza.testmvvm.viewmodel;

import android.content.Context;

import alooza.testmvvm.model.Country;

/**
 * Created by Azim on 06/01/2017.
 */

public class CountryDetailsViewModel implements ViewModel {

    private static final String TAG = "CountryDetailsViewModel";

    private Context context;
    private Country country;

    public CountryDetailsViewModel(Context context, Country country) {
        this.context = context;
        this.country = country;
    }

    public String getName() {
        return country.name;
    }

    public String getCapital() {
        return country.capital;
    }

    public String getRegion() {
        return country.region;
    }

    public String getSubRegion() {
        return country.subRegion;
    }

    public String getPopulation() {
        return String.valueOf(country.population);
    }

    @Override
    public void destroy() {
        this.context = null;
    }

}
